#!/user/bin/env groovy

def call(){
    sh "npm version minor"
              def packageJSON = readJSON file: 'package.json'
              def packageJSONVersion = packageJSON.version
              env.IMAGE_NAME = "$packageJSONVersion-$BUILD_NUMBER"
}