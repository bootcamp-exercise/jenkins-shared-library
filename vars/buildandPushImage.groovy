#!/user/bin/env groovy

def call(){
    withCredentials([
          usernamePassword(credentialsId: 'soni-dockerhub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
        ]) {

          sh "docker build -t sathyavathi11/nodejs:${IMAGE_NAME} ."
          sh "echo $PASSWORD docker login - u $USERNAME --password-stdin"
          sh "docker push sathyavathi11/nodejs:${IMAGE_NAME}"
        }
}