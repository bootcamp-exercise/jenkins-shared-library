#!/user/bin/env groovy


def call(){

    withCredentials([
            usernamePassword(credentialsId: 'soni-jenkins', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
          ]) {
            sh 'git config --global user.email "sathyavathi.da@gmail.com"'
            sh 'git config --global user.name "soni karthik"'
            sh 'git remote set-url origin https://$USERNAME:$PASSWORD@gitlab.com/bootcamp-exercise/build-automation-and-cicd-with-jenkins.git'
            sh 'git add .'
            sh 'git commit -m "Version update commit"'
            sh 'git push origin HEAD:master'
          }
}
